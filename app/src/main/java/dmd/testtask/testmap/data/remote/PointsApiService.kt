package dmd.testtask.testmap.data.remote

import dmd.testtask.testmap.data.entity.PointEntity
import io.reactivex.Observable
import retrofit2.http.GET

interface PointsApiService {
    @GET("get/bPvorlFkwO?indent=2")
    fun points(): Observable<List<PointEntity>>
}