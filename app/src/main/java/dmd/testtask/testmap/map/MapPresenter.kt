package dmd.testtask.testmap.map

import android.os.Bundle
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dmd.testtask.testmap.data.model.Point
import dmd.testtask.testmap.data.repository.PointsRepository

class MapPresenter(private val mapView: MapView) {
    companion object {
        private const val TAG = "MapPresenter"

        private const val SELECTED_INFO_KEY = "selected_info_key"
        private const val CAMERA_POSITION_KEY = "camera_position_key"
    }

    private lateinit var map: GoogleMap

    private val markers: MutableList<Marker> = ArrayList()

    private var selectedInfo: PointInfoArgs? = null
    private var restoredCameraPosition: CameraPosition? = null

    fun startWithMap(map: GoogleMap) {
        this.map = map
        map.setOnMarkerClickListener {
            this.selectedInfo = PointInfoArgs(it.title, it.snippet)
            showPointInfo()
            true
        }
        map.setOnMapClickListener {
            this.selectedInfo = null
            this.mapView.hidePointInfo()
        }

        loadPoints()

        restoredCameraPosition?.let { map.moveCamera(CameraUpdateFactory.newCameraPosition(it)) }
    }

    private fun showPointInfo() {
        this.selectedInfo?.let { this.mapView.showPointInfo(it) }
    }

    private fun loadPoints() {
        PointsRepository.instance.points()
                .subscribe({ processedMarkers(it) }) { Log.e(TAG, "error", it) }
    }

    private fun processedMarkers(points: List<Point>) {
        this.markers.clear()
        points.forEach {
            val marker = this.map.addMarker(createMarkerFromPoint(it))
            this.markers.add(marker)
        }

        moveMapCameraToFirstMarker(points.firstOrNull())
    }

    private fun moveMapCameraToFirstMarker(point: Point?) {
        if (point == null) return
        val position = LatLng(point.latitude, point.longitude)
        this.map.moveCamera(CameraUpdateFactory.newLatLng(position))
    }

    private fun createMarkerFromPoint(point: Point): MarkerOptions {
        val position = LatLng(point.latitude, point.longitude)
        return MarkerOptions().position(position).title(point.title).snippet(point.description)
    }

    fun onSaveInstanceState(outState: Bundle?) {
        outState?.putParcelable(SELECTED_INFO_KEY, selectedInfo)
        outState?.putParcelable(CAMERA_POSITION_KEY, this.map.cameraPosition)
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        this.selectedInfo = savedInstanceState?.getParcelable(SELECTED_INFO_KEY)
        showPointInfo()

        this.restoredCameraPosition = savedInstanceState?.getParcelable(CAMERA_POSITION_KEY)
    }

    fun clearSelected() {
        this.selectedInfo = null
    }
}