package dmd.testtask.testmap.data.repository

import dmd.testtask.testmap.data.entity.PointEntity
import dmd.testtask.testmap.data.model.Point
import dmd.testtask.testmap.data.remote.ApiFactory
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PointsRepository private constructor() {
    private object Holder {
        val INSTANCE = PointsRepository()
    }

    companion object {
        val instance: PointsRepository by lazy { Holder.INSTANCE }
    }

    private var cachedPoints: List<Point>? = null

    fun points(): Observable<List<Point>> {
        if (this.cachedPoints != null) {
            return Observable.just(this.cachedPoints)
        }
        return ApiFactory.intstance.pointsService.points()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.map { mapEntityToDomain(it) } }
                .doOnNext { refreshCache(it) }
    }

    private fun refreshCache(points: List<Point>) {
        this.cachedPoints = points
    }

    private fun mapEntityToDomain(entity: PointEntity): Point = Point(
            entity.id,
            entity.title,
            entity.description,
            entity.lat,
            entity.lng
    )
}