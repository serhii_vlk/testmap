package dmd.testtask.testmap.data.entity

import com.google.gson.annotations.SerializedName

class PointEntity(
        @SerializedName("id")
        val id: Long,
        @SerializedName("title")
        val title: String,
        @SerializedName("description")
        val description: String,
        @SerializedName("lat")
        val lat: Double,
        @SerializedName("lng")
        val lng: Double
)