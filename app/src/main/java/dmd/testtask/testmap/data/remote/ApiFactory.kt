package dmd.testtask.testmap.data.remote

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiFactory private constructor() {
    private object Holder {
        val INSTANCE = ApiFactory()
    }

    companion object {
        val intstance: ApiFactory by lazy { Holder.INSTANCE }
    }

    private val client = OkHttpClient.Builder()
            .build()

    private val retrofit = Retrofit.Builder()
            .client(this.client)
            .baseUrl("http://www.json-generator.com/api/json/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    val pointsService: PointsApiService
        get() = this.retrofit.create(PointsApiService::class.java)
}