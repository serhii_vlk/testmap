package dmd.testtask.testmap.map

import android.os.Parcel
import android.os.Parcelable

data class PointInfoArgs(
        val title: String,
        val description: String
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(title)
        writeString(description)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<PointInfoArgs> = object : Parcelable.Creator<PointInfoArgs> {
            override fun createFromParcel(source: Parcel): PointInfoArgs = PointInfoArgs(source)
            override fun newArray(size: Int): Array<PointInfoArgs?> = arrayOfNulls(size)
        }
    }
}