package dmd.testtask.testmap.map

interface MapView {
    fun showPointInfo(info: PointInfoArgs)

    fun hidePointInfo()
}