package dmd.testtask.testmap.data.model

data class Point(
        val id: Long,
        val title: String,
        val description: String,
        val latitude: Double,
        val longitude: Double
)