package dmd.testtask.testmap.map

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AppCompatActivity
import android.view.View

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment

import dmd.testtask.testmap.R
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.point_description_bottom_sheet.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback, MapView {
    private lateinit var presenter: MapPresenter

    private lateinit var descriptionBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        this.presenter = MapPresenter(this)

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        this.descriptionBehavior = BottomSheetBehavior.from(bottom_sheet_container)
        this.descriptionBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    presenter.clearSelected()
                }
            }

        })
    }

    override fun showPointInfo(info: PointInfoArgs) {
        point_title.text = info.title
        point_description.text = info.description
        this.descriptionBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun hidePointInfo() {
        this.descriptionBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.presenter.startWithMap(googleMap)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        this.presenter.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        this.presenter.onRestoreInstanceState(savedInstanceState)
        super.onRestoreInstanceState(savedInstanceState)
    }
}
